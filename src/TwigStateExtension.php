<?php

namespace Drupal\twig_state_access;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\State;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Theme\ThemeManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig state extensions.
 */
class TwigStateExtension extends AbstractExtension {

  /**
   * Drupal state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Private temp store factory service.
   *
   * @var PrivateTempStoreFactory
   */
  protected $privateTempStoreFactory;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Twig state constructor.
   */
  public function __construct(State $state, PrivateTempStoreFactory $privateTempStoreFactory, ModuleHandlerInterface $module_handler, ThemeManagerInterface $theme_manager) {
    $this->moduleHandler = $module_handler;
    $this->themeManager = $theme_manager;
    $this->state = $state;
    $this->privateTempStoreFactory = $privateTempStoreFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    $functions = [
      new TwigFunction('drupal_state', [$this, 'drupalState']),
      new TwigFunction('drupal_private_state', [$this, 'drupalPrivateState']),
    ];

    $this->moduleHandler->alter('twig_state_access_functions', $functions);
    $this->themeManager->alter('twig_state_access_functions', $functions);

    return $functions;
  }

  /**
   * Get related given key(s) value(s) from state.
   *
   * @param string|array $key
   *   The key of the data to retrieve.
   * @param mixed $default
   *   The default value.
   *
   * @return mixed
   *    The data associated with the key, or NULL if the key does not exist.
   */
  public function drupalState(string|array $key, mixed $default = NULL): mixed {
    if (is_array($key)) {
      return $this->state->getMultiple($key);
    }
    return $this->state->get($key, $default);
  }

  /**
   * Get related given key(s) value(s) from state.
   *
   * @param string $collection
   *   The collection name to use for this key/value store. This is typically
   *   a shared namespace or module name, e.g. 'views', 'entity', etc.
   * @param string $key
   *   The key of the data to retrieve.
   * @param bool $metadata
   *   Boolean value when TRUE then return given key related metadata.
   *
   * @return mixed
   *   The data associated with the key, or NULL if the key does not exist.
   */
  public function drupalPrivateState(string $collection, string $key, $metadata = FALSE): mixed {
    if ($metadata) {
      return $this->privateTempStoreFactory->get($collection)->getMetadata($key);
    }
    return $this->privateTempStoreFactory->get($collection)->get($key);
  }

}
